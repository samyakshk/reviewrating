<?php

namespace GeniusSystems\ReviewRating\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;


class ReviewRating extends Model
{

    protected $table = 'review_ratings';

    protected $guarded = ['id', 'created_at', 'updated_at'];

  
}
