<?php

namespace GeniusSystems\ReviewRating\Models;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Comment extends Model
{


    protected $table = 'content_comments';

    protected $fillable = ['id', 'user_id', 'username', 'content_type', 'content_id', 'comment', 'parent_id'];
  
  
  

    public function replies()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }
}
