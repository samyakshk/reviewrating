<?php

namespace GeniusSystems\ReviewRating\Models;

use Illuminate\Database\Eloquent\Model;


class ReviewSettings extends Model
{


    protected $table = 'review_settings';

    protected $guarded = ['id', 'only_approved', 'enabled_features'];
}
