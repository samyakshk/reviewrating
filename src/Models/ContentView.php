<?php


namespace GeniusSystems\ReviewRating\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class ContentView extends Model
{
    protected $table = 'content_views';

    protected $guarded = ['id','created_at','updated_at'];

 
}
