<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
        setting endpoint
    */

Route::group(['prefix' => 'settings/'], function () {
    Route::patch('{id}', 'GeniusSystems\ReviewRating\Http\Controllers\SettingsController@update');
    Route::get('/', 'GeniusSystems\ReviewRating\Http\Controllers\SettingsController@settings');
});
Route::group(['prefix' => 'type/{type}/contents/{content_id}'], function () {
    Route::get('/ratings/-/average', 'GeniusSystems\ReviewRating\Http\Controllers\ReviewRatingController@showAverageRating');
    Route::get('ratings/-/total/', 'GeniusSystems\ReviewRating\Http\Controllers\ReviewRatingController@showTotalRating');
    Route::get('/recommendations/-/total', 'GeniusSystems\ReviewRating\Http\Controllers\ReviewRatingController@showTotalRecommend');
    Route::get('/likes', 'GeniusSystems\ReviewRating\Http\Controllers\ContentLikeController@countLikes');
    Route::get('/dislikes', 'GeniusSystems\ReviewRating\Http\Controllers\ContentLikeController@countDislikes');

    Route::get('/views', 'GeniusSystems\ReviewRating\Http\Controllers\ContentViewController@countViews');

    Route::get('/reviews', 'GeniusSystems\ReviewRating\Http\Controllers\ReviewRatingController@index');
    Route::post('/reviews', 'GeniusSystems\ReviewRating\Http\Controllers\ReviewRatingController@store');
    Route::post('/likes', 'GeniusSystems\ReviewRating\Http\Controllers\ContentLikeController@like');
    Route::post('/dislikes', 'GeniusSystems\ReviewRating\Http\Controllers\ContentLikeController@dislike');
    Route::post('/views', 'GeniusSystems\ReviewRating\Http\Controllers\ContentViewController@store');
    Route::patch('/reviews/{review_id}/approve', 'GeniusSystems\ReviewRating\Http\Controllers\ReviewRatingController@approveReview');
   
    Route::post('/comments', 'GeniusSystems\ReviewRating\Http\Controllers\CommentsController@store');
    Route::patch('/comments/{comment_id}', 'GeniusSystems\ReviewRating\Http\Controllers\CommentsController@update');
    Route::delete('/comments/{comment_id}', 'GeniusSystems\ReviewRating\Http\Controllers\CommentsController@delete');
    Route::get('/comments', 'GeniusSystems\ReviewRating\Http\Controllers\CommentsController@getByContentId');
   
    /*
        content detail endpoint
    */
    Route::get('/details', 'GeniusSystems\ReviewRating\Http\Controllers\ContentDetailController@details');
    

    Route::group(['prefix' => 'users/{username}'], function () {
        Route::patch('/reviews/{review_id}', 'GeniusSystems\ReviewRating\Http\Controllers\ReviewRatingController@update');
        Route::delete('/reviews/{review_id}', 'GeniusSystems\ReviewRating\Http\Controllers\ReviewRatingController@destroy');

    });
   
});
Route::get('/users/{user_id}/type/{type}/contents/{content_id}/likes', 'GeniusSystems\ReviewRating\Http\Controllers\ContentLikeController@getLikeByUserIdAndContent');