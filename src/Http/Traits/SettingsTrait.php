<?php

namespace GeniusSystems\ReviewRating\Http\Traits;

use Illuminate\Support\Facades\App;
use GeniusSystems\ReviewRating\Repository\Interfaces\SettingsInterface;

trait SettingsTrait
{
    public function getSettingsData()
    {
        $settingsInterface = App::make(SettingsInterface::class);
        $settings = $settingsInterface->getSettingsData();
        return $settings;
    }

    public function getEnabledFeatures(){   
        $settingsInterface = App::make(SettingsInterface::class);
        return $settingsInterface->getEnabledFeatures();
    }

    public function reviewSetting(){
        $settingsInterface = App::make(SettingsInterface::class);
        return $settingsInterface->reviewSetting();
    }
}
