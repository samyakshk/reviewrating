<?php


namespace GeniusSystems\ReviewRating\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GeniusSystems\ReviewRating\Http\Traits\SettingsTrait;
use GeniusSystems\ReviewRating\Repository\Interfaces\SettingsInterface;



class SettingsController extends Controller
{
    private $settings;
    /**
     * @var string
     */
    use SettingsTrait;
    public function __construct(SettingsInterface $settings)
    {
        $this->settings = $settings;
    }

    public function update($id, Request $request)
    {
        $this->context = 'update setting';
        try {
            $this->validate($request, [
                'value'       => 'required|string|in:0,1',

            ]);
        } catch (\Exception $ex) {
            return $this->message($ex->response->original, 422, $this->context);
        }
        try {
            
            $this->settings->update($id, $request->all());
            return $this->message('Settings updated successfully', 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 404, $this->context, $ex->getMessage());
        } catch (QueryException $exception) {
            return $this->message('sql error', 521, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }
    public function settings(){
        try {
            
            $data = $this->settings->getSettingsData();
            return $this->response($data, 200,null);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 404, $this->context, $ex->getMessage());
        } catch (QueryException $exception) {
            return $this->message('sql error', 521, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }
}
