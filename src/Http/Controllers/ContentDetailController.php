<?php


namespace GeniusSystems\ReviewRating\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GeniusSystems\ReviewRating\Http\Traits\SettingsTrait;
use GeniusSystems\ReviewRating\Repository\Interfaces\CommentInterface;
use GeniusSystems\ReviewRating\Repository\Interfaces\ContentLikeInterface;
use GeniusSystems\ReviewRating\Repository\Interfaces\ContentViewInterface;
use GeniusSystems\ReviewRating\Repository\Interfaces\ReviewRatingInterface;

class ContentDetailController extends Controller
{
    private $contentView, $context;
    /**
     * @var string
     */
    use SettingsTrait;
    public function __construct(ContentViewInterface $contentView, ContentLikeInterface $contentLike, ReviewRatingInterface $review,CommentInterface $comment)
    {
        $this->contentView = $contentView;
        $this->contentLike = $contentLike;
        $this->review = $review;
        $this->comment = $comment;
    }

    public function details($type, $content_id)
    {
        try {

            $details = [];


            $views = $this->contentView->getTotalViews($type, $content_id);
            $details['views'] = $views;


            $likes = $this->contentLike->getTotalLikes($type, $content_id);
            $details['likes'] = $likes;


            $dislikes = $this->contentLike->getTotalDislikes($type, $content_id);
            $details['dislikes'] = $dislikes;
            $comments =  $this->comment->getByContentId($content_id, $type);
            $ratings_reviews = $this->review->getReviewsAndRatings($type, $content_id);
          
            if ($details['views'] == 0 && $details['likes'] == 0 && $details['dislikes'] == 0 && !$ratings_reviews) {
                return $this->message('Resource not found.', 204, $this->context, 'Resource not found.');
            }
            return array_merge($details, $ratings_reviews, $comments);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, 'Resource not found.');
        } catch (QueryException $exception) {
            return $this->message('sql error', 521, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }
}
