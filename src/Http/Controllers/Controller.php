<?php


namespace GeniusSystems\ReviewRating\Http\Controllers;



use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

    public function response($data, $code, $context)
    {
        return response()->json($data, $code);
    }

    public function message($message, $code, $context, $messageOverride = '')
    {
        $message = isset($message) && $messageOverride != '' ? $messageOverride : $message;
        return response()->json(['message' => $message], $code);
    }
}
