<?php


namespace GeniusSystems\ReviewRating\Http\Controllers;

use Illuminate\Http\Request;


use GeniusSystems\ReviewRating\Models\Comment;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use GeniusSystems\ReviewRating\Repository\Interfaces\CommentInterface;

class CommentsController extends Controller
{
    public function __construct(CommentInterface $comment)
    {
        $this->comment = $comment;
    }
    public function store($type, $content_id, Request $request)
    {
        $this->context = "post comments";
        try {
            $this->validate($request, [

                'comment' => 'required',
                'username' => 'required' 

            ]);
        } catch (\Exception $ex) {
            return $this->message($ex->response->original, 422, $this->context);
        }
        try {
            $create = $request->all();

            $create['content_type'] = $type;
            $create['content_id'] = $content_id;
           
            if(isset($request['parent_id']) && $request['parent_id'] != null ){
                /* 
                    not allow reply of replies
                */
                    $parent = $this->comment->getById($request['parent_id']);
                    if($parent['parent_id']){
                        return $this->message("Not allowed to post reply of reply", 422, $this->context);
                    }
            }
            $this->comment->create($create);
            return $this->message('Comment Successfully posted.', 200, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, $ex->getMessage());
        }
    }
    public function update($type, $content_id, $comment_id, Request $request)
    {
        $this->context = "update comments";
        try {
            $this->validate($request, [

                'comment' => 'required',
                'username' => 'required'

            ]);
        } catch (\Exception $ex) {
            return $this->message($ex->response->original, 422, $this->context);
        }
        try {

            $create = $request->all();
          
            $create['content_type'] = $type;
            $create['content_id'] = $content_id;
            $this->comment->update($comment_id,$create);
            return $this->message('Comment Successfully updated.', 200, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, $ex->getMessage());
        }
    }
    public function delete($type, $content_id, $comment_id)
    {
        $this->context = 'Delete Comment';
        try {
            $data = $this->comment->getById($comment_id);
            if (!$data) {
                return $this->message('Comment not found', 204, $this->context, 'Comment Not found');
            }
          
            $this->comment->delete($data->id);
          
            return $this->message('Comment Successfully deleted.', 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, 'Resource Not found');
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }
    public function getByContentId($type, $content_id)
    {
        $this->context = 'Comments by Content';
        try {
            $data = $this->comment->getByContentId($content_id,$type);
            
            return $this->response($data, 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, 'Resource Not found');
        } catch (\Exception $ex) {
          
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }

}
