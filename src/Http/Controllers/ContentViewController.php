<?php


namespace GeniusSystems\ReviewRating\Http\Controllers;


use GeniusSystems\ReviewRating\Repository\Interfaces\ContentViewInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContentViewController extends Controller
{
    private $contentView, $context;
    /**
     * @var string
     */

    public function __construct(ContentViewInterface $contentView)
    {
        $this->contentView=$contentView;
    }

    public function store($type,$content_id,Request $request)
    {
        $this->context = 'View Content';
        try {
            $this->validate($request, [
                'user_id'      => 'required',
                'content_slug'      => 'required',
                'username'          => 'required'
            ]);
        } catch (\Exception $ex) {
            return $this->message($ex->response->original, 422, $this->context);
        }
        try {
            DB::beginTransaction();
            $data = [
                'like'          => (string)$request['like'],
                'user_id'       => $request['user_id'],
                'viewable_type'  => $type,
                'viewable_id'    => $content_id,
                'ip_address'    => $request->getClientIp(),
                'agent'         => $request->header('User-Agent'),
                'username'      => $request['username'],
                'content_slug'  => $request['content_slug'],
            ];
            $this->contentView->create($data);
            DB::commit();
            return $this->message('View Created successfully.', 201, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, $ex->getMessage());
        }
    }

    public function countViews($type,$content_id)
    {
        $this->context = 'Views in content';
        try {
            $data = $this->contentView->getTotalViews($type,$content_id);
            return $this->response($data, 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, 'Resource not found.');
        } catch (QueryException $exception) {
            return $this->message('sql error', 521, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }
}
