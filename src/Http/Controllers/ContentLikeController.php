<?php

namespace GeniusSystems\ReviewRating\Http\Controllers;

use GeniusSystems\ReviewRating\Repository\Interfaces\ContentLikeInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContentLikeController extends Controller
{
    private $contentLike, $context;

    public function __construct(ContentLikeInterface $contentLike)
    {
        $this->contentLike = $contentLike;
    }

    public function store($type, $content_id, Request $request)
    {

        $this->context = 'Like Content';
        try {
            DB::beginTransaction();

            $data = [
                'like'          => (string)$request['like'],
                'user_id'       => $request['user_id'],
                'likable_type'  => $type,
                'likable_id'    => $content_id,
                'ip_address'    => $request->getClientIp(),
                'agent'         => $request->header('User-Agent'),
                'user_id'           => 'required',
                'content_slug'      => 'required',
                'username'          => 'required'
            ];
            $this->contentLike->create($data);
            DB::commit();
            return $this->message('Liked Successfully.', 201, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, $ex->getMessage());
        }
    }

    public function update($id, $user_id, $like_id, Request $request)
    {
        $data = $this->contentLike->getSpecificById($user_id, $like_id);
        $this->context = 'Update Review and Rating';
        try {
            $this->validate($request, [
                'likable_type'      => 'required',
            ]);
        } catch (\Exception $ex) {
            return $this->message($ex->response->original, 422, $this->context);
        }
        try {
            DB::beginTransaction();
            $update = [
                'like'          => (string)$request['like'],
                'user_id'       => $user_id,
                'likable_type'  => $request['likable_type'],
                'likable_id'    => $id,
                'ip_address'    => $request->getClientIp(),
                'agent'         => $request->header('User-Agent'),
                'content_slug'      => 'required',
                'username'          => 'required'
            ];
            $this->contentLike->update($data->id, $update);
            DB::commit();
            return $this->message('Updated.', 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 404, $this->context, $ex->getMessage());
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }

    public function destroy($id, $user_id, $like_id)
    {
        $this->context = 'Undo Like';
        try {
            $data = $this->contentLike->getSpecificById($user_id, $like_id);
            DB::beginTransaction();
            $this->contentLike->delete($data->id);
            DB::commit();
            return $this->message('Deleted.', 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 404, $this->context, 'Resource Not found');
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }

    public function like($type, $content_id, Request $request)
    {
        try {
            $this->validate($request, [
                'user_id'      => 'required',
                'content_slug'      => 'required',
                'username'          => 'required'

            ]);
        } catch (\Exception $ex) {
            return $this->message($ex->response->original, 422, $this->context);
        }
        try {
            $user_like = $this->contentLike->getLikeByUserIdAndContent($request['user_id'], $type, $content_id);

            if ($user_like) {
                if ($user_like->like == "1") {
                    $like = "0";
                } elseif ($user_like->like == "0") {
                    $like = "1";
                } elseif ($user_like->like == "2") {
                    $like = "1";
                }
            } else {
                $like = "1";
            }
            DB::beginTransaction();

            $data = [
                'like'          => $like,
                'user_id'       => $request['user_id'],
                'likable_type'  => $type,
                'likable_id'    => $content_id,
                'ip_address'    => $request->getClientIp(),
                'agent'         => $request->header('User-Agent'),
                'content_slug'  => $request['content_slug'],
                'username'      => $request['username']
            ];
            if (!$user_like) {
                $this->contentLike->create($data);
            } else {
                $this->contentLike->update($user_like->id, $data);
            }

            DB::commit();
            return $this->message('Liked Successfully.', 201, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, $ex->getMessage());
        }
    }
    public function dislike($type, $content_id, Request $request)
    {
        try {
            $this->validate($request, [
                'user_id'      => 'required',
                'content_slug'      => 'required',
                'username'          => 'required'
            ]);
        } catch (\Exception $ex) {
            return $this->message($ex->response->original, 422, $this->context);
        }
        try {
            $user_like = $this->contentLike->getLikeByUserIdAndContent($request['user_id'], $type, $content_id);

            if ($user_like) {
                if ($user_like->like == "2") {
                    $like = "0";
                } elseif ($user_like->like == "0") {
                    $like = "2";
                } elseif ($user_like->like == "1") {
                    $like = "2";
                }
            } else {
                $like = "2";
            }
            DB::beginTransaction();

            $data = [
                'like'          => $like,
                'user_id'       => $request['user_id'],
                'likable_type'  => $type,
                'likable_id'    => $content_id,
                'ip_address'    => $request->getClientIp(),
                'agent'         => $request->header('User-Agent'),
                'content_slug'  => $request['content_slug'],
                'username'      => $request['username']
            ];
            if (!$user_like) {
                $this->contentLike->create($data);
            } else {
                $this->contentLike->update($user_like->id, $data);
            }

            DB::commit();
            return $this->message('Disliked Successfully.', 201, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, $ex->getMessage());
        }
    }
    public function countLikes($type, $content_id)
    {
        $this->context = 'Likes in content';
        try {
            $data = $this->contentLike->getTotalLikes($type, $content_id);
            return $this->response($data, 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, 'Resource not found.');
        } catch (QueryException $exception) {
            return $this->message('sql error', 521, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }
    public function countDislikes($type, $content_id)
    {
        $this->context = 'Disikes in content';
        try {
            $data = $this->contentLike->getTotalDislikes($type, $content_id);
            return $this->response($data, 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, 'Resource not found.');
        } catch (QueryException $exception) {
            return $this->message('sql error', 521, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }
    public function getLikeByUserIdAndContent($user_id, $type, $content_id)
    {
        try {
            $data = $this->contentLike->getLikeByUserIdAndContent($user_id, $type, $content_id);
            if($data['like'] == "1"){
                $result['liked'] = true;
            }elseif($data['like'] == "2"){
                $result['disliked'] = true;
            }else{
                $result = null ;
            }
            return $this->response($result, 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, 'Resource not found.');
        } catch (QueryException $exception) {
            return $this->message('sql error', 521, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }
}
