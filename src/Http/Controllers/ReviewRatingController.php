<?php

namespace GeniusSystems\ReviewRating\Http\Controllers;

use GeniusSystems\ReviewRating\Repository\Interfaces\ReviewRatingInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;


class ReviewRatingController extends Controller
{
    private $review, $context;
    /**
     * @var string
     */

    public function __construct(ReviewRatingInterface $review)
    {
        $this->review = $review;
    }

    public function index($id)
    {
        $this->context = 'Review and Rating of a content';
        try {
            $request = App::make(Request::class);
            $approved = $request->input('approved');
            $sort = $request->input('sort');
            $data = $this->review->getContentReviews($id, $sort, $approved);
            return $this->response($data, 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, 'Resource not found.');
        } catch (QueryException $exception){
            return $this->message('sql error', 521, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }

    public function store($type,$content_id, Request $request)
    {   
        $this->context = 'Add Review and Rating';
        try {
            $this->validate($request, [
                'is_approved'       => 'sometimes|in:0,1',
                'rating'            => 'sometimes|nullable|max:5',
                'recommend'         => 'sometimes|nullable|in:yes,no',
                'user_id'           => 'required',
                'content_slug'      => 'required',
                'username'          => 'required'
            ]);
        } catch (\Exception $ex) {
            return $this->message($ex->response->original, 422, $this->context);
        }
        try {
            DB::beginTransaction();
            $create = $request->all();
            if($request->filled('is_approved')){
                $create['is_approved'] = (string)$request['is_approved'];
            }
            $create['reviewable_type'] = $type;
            $create['reviewable_id'] = $content_id;
            $this->review->create($create);
            DB::commit();
            return $this->message('Review Successfully posted.', 200, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, $ex->getMessage());
        }
    }

    public function update($type,$content_id, $username, $review_id, Request $request)
    {
        $data = $this->review->getSpecificById($username, $review_id);
        if(!$data){
            return $this->message("Review not found.", 204, $this->context, "Review not found.");
        }
        $this->context = 'Update Review and Rating';
        try {
            $this->validate($request, [
                'is_approved'       => 'sometimes|in:0,1',
                'rating'            => 'sometimes|nullable|max:5',
                'recommend'         => 'sometimes|nullable|in:yes,no',
                'user_id'           => 'required',
                'content_slug'      => 'required',
                'username'          => 'required'
            ]);
        } catch (\Exception $ex) {
            return $this->message($ex->response->original, 422, $this->context);
        }
        try {
            DB::beginTransaction();

            $update = [
                'is_approved'       => (string)$data['is_approved'],
                'rating'            => $request['rating'],
                'review'            => $request['review'],
                'recommend'         => $request['recommend'],
                'reviewable_id'     => $content_id,
                'reviewable_type'   => $type,
                'user_id'           => $request['user_id'],
                'username'          => $username,
                ];
            $this->review->update($data->id,$update);
            DB::commit();
            return $this->message('Review updated Successfully.', 200, $this->context);
        } catch (ModelNotFoundException $ex){
            return $this->message($ex->getMessage(), 204, $this->context, $ex->getMessage());
        }catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }

    public function destroy($type,$content_id,$username, $review_id)
    {
        $this->context = 'Delete Review and Rating';
        try {
            $data = $this->review->getSpecificById($username, $review_id);
            if(!$data){
                return $this->message('Review not found', 204, $this->context, 'Review Not found');
            }
            DB::beginTransaction();
            $this->review->delete($data->id);
            DB::commit();
            return $this->message('Review Successfully deleted.', 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, 'Resource Not found');
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }

    public function showAverageRating($type,$content_id)
    {
        $this->context = 'Average Ratings';
        $request = App::make(Request::class);
        $approve = $request->input('approved');

        try {
            $data = $this->review->averageContentRating($type,$content_id,$approve);
            return $this->response($data, 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, 'Resource not found.');
        } catch (QueryException $exception){
            return $this->message('sql error', 521, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }

    public function showTotalRating($type,$content_id)
    {
        $this->context = 'Total Ratings';
        $request = App::make(Request::class);
        $approve = $request->input('approved');

        try {
            $data = $this->review->countContentRating($type,$content_id,$approve);
            return $this->response($data, 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, 'Resource not found.');
        } catch (QueryException $exception){
            return $this->message('sql error', 521, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }

    public function showTotalRecommend($type,$content_id)
    {
        $this->context = 'Total Ratings';
        $request = App::make(Request::class);
        $approve = $request->input('approved');
        try {
            $data = $this->review->countRecommendedContent($type,$content_id,$approve);
            return $this->response($data, 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, 'Resource not found.');
        } catch (QueryException $exception){
            return $this->message('sql error', 521, $this->context);
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }
    public function approveReview($type, $content_id, $review_id, Request $request)
    {
        try {
            $this->validate($request, [
                'is_approved'       => 'required|in:0,1',

            ]);
        } catch (\Exception $ex) {
            return $this->message($ex->response->original, 422, $this->context);
        }
        try {
            $data = $this->review->getSpecificByReviewId($review_id);
            $update = [
                'is_approved'       => (string)$request['is_approved'],
            ];
            $this->review->update($data->id, $update);
            return $this->message('Review updated Successfully.', 200, $this->context);
        } catch (ModelNotFoundException $ex) {
            return $this->message($ex->getMessage(), 204, $this->context, $ex->getMessage());
        } catch (\Exception $ex) {
            return $this->message($ex->getMessage(), 500, $this->context, 'Something went wrong');
        }
    }
}
