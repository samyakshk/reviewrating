<?php


namespace GeniusSystems\ReviewRating\Repository\Interfaces;


interface SettingsInterface extends BaseInterface
{
    public function getSettingsData();
    public function getEnabledFeatures();

}

