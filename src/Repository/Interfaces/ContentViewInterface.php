<?php


namespace GeniusSystems\ReviewRating\Repository\Interfaces;


interface ContentViewInterface extends BaseInterface
{
    public function getTotalViews($type,$contentId);
}
