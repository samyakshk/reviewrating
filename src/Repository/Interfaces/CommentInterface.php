<?php


namespace GeniusSystems\ReviewRating\Repository\Interfaces;


interface CommentInterface extends BaseInterface
{
  
    public function getById($comment_id);
    public function getByContentId($content_id,$type);
  
}
