<?php


namespace GeniusSystems\ReviewRating\Repository\Interfaces;


interface ReviewRatingInterface extends BaseInterface
{
    public function getContentReviews($content_id, $sort, $approved);

    public function averageContentRating($type,$content_id, $approved);

    public function countContentRating($type,$content_id, $approved);

    public function countRecommendedContent($type,$content_id, $approved);

    public function getReviewsAndRatings($type,$content_id);
    public function getSpecificByReviewId($review_id);
}

