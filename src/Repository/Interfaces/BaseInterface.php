<?php


namespace GeniusSystems\ReviewRating\Repository\Interfaces;


interface BaseInterface
{
    public function getSpecificById($username, $id);

    public function create(array $data);

    public function update($id, array $data);

    public function delete($id);
}
