<?php


namespace GeniusSystems\ReviewRating\Repository\Interfaces;


interface ContentLikeInterface extends BaseInterface
{
   public function getTotalLikes($type,$content_id);
   public function getTotalDislikes($type,$content_id);
   public function getLikeByUserIdAndContent($user_id,$type,$content_id);
}
