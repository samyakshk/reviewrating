<?php


namespace GeniusSystems\ReviewRating\Repository\Eloquent;


use GeniusSystems\ReviewRating\Repository\Interfaces\BaseInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class BaseRepo implements BaseInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getSpecificById($username, $id)
    {
        return $this->model
            ->where([['username',$username],['id',$id]])
            ->first();
    }


    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update($id, array $data)
    {
        
        $this->model->findOrFail($id)->update($data);
        $data = $this->model->findOrFail($id);
        return $data;
    }

    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }
}
