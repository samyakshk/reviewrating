<?php


namespace GeniusSystems\ReviewRating\Repository\Eloquent;


use GeniusSystems\ReviewRating\Models\ContentView;
use GeniusSystems\ReviewRating\Repository\Interfaces\ContentViewInterface;
use Illuminate\Support\Facades\Validator;

class ContentViewRepo extends BaseRepo implements ContentViewInterface
{
    private $contentView;

    public function __construct(ContentView $contentView)
    {
        $this->contentView = $contentView;
        parent::__construct($contentView);
    }

    public function getTotalViews($type,$contentId)
    {
        return $this->contentView
            ->where('viewable_type',$type)
            ->where('viewable_id',$contentId)
            ->count();
    }
}

