<?php

namespace  GeniusSystems\ReviewRating\Repository\Eloquent;

use GeniusSystems\ReviewRating\Models\ReviewRating;
use  GeniusSystems\ReviewRating\Repository\Interfaces\ReviewRatingInterface;
use GeniusSystems\ReviewRating\Http\Traits\SettingsTrait;

class ReviewRatingRepo extends BaseRepo implements ReviewRatingInterface
{
    private $review;
    use SettingsTrait;
    public function __construct(ReviewRating $review)
    {
        $this->review = $review;
        parent::__construct($review);
    }

    public function getContentReviews($id, $sort = "desc", $approved = false)
    {
        $sorting = $sort ? $sort : 'desc';
        $where = $approved ? [['is_approved', 1]] : [];
        return $this->review
            ->where('reviewable_id', $id)
            ->where($where)
            ->orderBy('created_at', $sorting)
            ->get();
    }

    public function averageContentRating($type, $content_id, $approved = false)
    {

        $where = $approved ? [['is_approved', "1"]] : [];

        $data = $this->review
            ->selectRaw('AVG(rating) as averageRating')
            ->where('reviewable_type', $type)
            ->where('reviewable_id', $content_id)
            ->where($where)
            ->pluck('averageRating')
            ->first();
        return $data;
    }

    public function countContentRating($type, $content_id, $approved = false)
    {
        $where = $approved ? [['is_approved', 1]] : [];
        return $this->review
            ->where('reviewable_id', $content_id)
            ->where('reviewable_type', $type)
            ->where($where)
            ->count();
    }

    public function countRecommendedContent($type, $content_id, $approved = false)
    {
        $where = $approved ? [['is_approved', 1]] : [];
        return $this->review
            ->where([['reviewable_id', $content_id], ['recommend', 'yes']])
            ->where('reviewable_type', $type)
            ->where($where)
            ->count();
    }

    public function getReviewsAndRatings($type, $content_id)
    {
        $only_show_approved_review = $this->reviewSetting();
        if ($only_show_approved_review == "1") {
            $approved = true;
        } else {
            $approved = false;
        }

        $where = $approved ? [['is_approved', "1"]] : [];
        $result = [];
        $data = $this->review
            ->where('reviewable_type', $type)
            ->where('reviewable_id', $content_id)
            ->where($where)
            ->get();
        $data_collection = collect($data);


        $result['average_ratings'] = $data_collection->avg('rating')?$data_collection->avg('rating'):0;
        $result['total_recommendations'] = $data_collection->where('recommend', 'yes')->count();
        $result['reviews'] = $data;
        if($result['average_ratings'] == 0 && $result['total_recommendations'] == 0 && $data->isEmpty()  ){
            $result = [];
        }
        return $result;
    }
    public function getSpecificByReviewId($review_id)
    {
        return $this->review
            ->where('id', $review_id)
            ->first();
    }
}
