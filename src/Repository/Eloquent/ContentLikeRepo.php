<?php


namespace GeniusSystems\ReviewRating\Repository\Eloquent;


use GeniusSystems\ReviewRating\Models\ContentLike;
use GeniusSystems\ReviewRating\Repository\Interfaces\ContentLikeInterface;

class ContentLikeRepo extends BaseRepo implements ContentLikeInterface
{
    private $contentLike;

    public function __construct(ContentLike $contentLike)
    {
        $this->contentLike = $contentLike;
        parent::__construct($contentLike);
    }

    public function getTotalLikes($type,$content_id)
    {
        return $this->contentLike
            ->where('likable_type',$type)
            ->where([['likable_id',$content_id],['like','1']])
            ->count();
    }

    public function getTotalDislikes($type,$content_id)
    {
        return $this->contentLike
            ->where('likable_type',$type)
            ->where([['likable_id',$content_id],['like','2']])
            ->count();
    }
    public function getLikeByUserIdAndContent($user_id,$type,$content_id){
        return $this->contentLike
            ->where('user_id',$user_id)
            ->where('likable_type',$type)
            ->where('likable_id',$content_id)
            ->first();
    }
}
