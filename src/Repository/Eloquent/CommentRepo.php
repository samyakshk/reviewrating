<?php


namespace GeniusSystems\ReviewRating\Repository\Eloquent;



use GeniusSystems\ReviewRating\Models\Comment;
use GeniusSystems\ReviewRating\Repository\Interfaces\CommentInterface;

class CommentRepo extends BaseRepo implements CommentInterface
{
  

    public function __construct(Comment $comments)
    {
        $this->model = $comments;
    }
    public function create(array $data)
    {
        return $this->model->create($data);
    }
    public function update($id, array $data)
    {
        return $this->model->findOrFail($id)->update($data);
    }
    public function getById($comment_id)
    {
        $data = Comment::findorfail($comment_id);
        return $data;
    }
    public function getByContentId($content_id, $type)
    {

        $comments = Comment::where('content_type', $type)
            ->where('content_id', $content_id)
            ->where('parent_id', null)
            ->with('replies')
            ->get();

       
        foreach ($comments as $comment) {
           
            if ($comment['replies']) {
                $replies_count = count($comment['replies']);
                
                $replies_count_array [] = $replies_count;
                
               
            }

            $comment['replies_count'] = $replies_count;
            
            
           
        }   
        if(isset($replies_count_array)){
            $sum_of_replies = array_sum($replies_count_array);
            $total_comment_count = count($comments) + $sum_of_replies;
    
        }else{
            $total_comment_count = 0;
        }
      
        $final_result = [
            "comments_count" => $total_comment_count,
            "comments" => $comments
        ];
        return $final_result;
    }
    public function getByContentIdbk($content_id, $type)
    {

        $comments = Comment::where('content_type', $type)
            ->where('content_id', $content_id)
            ->where('parent_id', null)
            ->with('replies')
            ->get();

       
        foreach ($comments as $comment) {
           
            if ($comment['replies']) {
                $replies_count = count($comment['replies']);
                
                $replies_count_array [] = $replies_count;
                
              if($replies_count != 0){
                foreach ($comment['replies'] as $reply_reply) {
                    $reply_reply_count = count($reply_reply['replies']);
                    $replies_count = $reply_reply_count + $replies_count;
                    $replies_count_array [] = $reply_reply_count;
                    foreach($reply_reply['replies'] as $level_3_reply){
                        unset($level_3_reply['replies']);
                    };
                }
              }
               
            }

            $comment['replies_count'] = $replies_count;
            
            
           
        }   
        if(isset($replies_count_array)){
            $sum_of_replies = array_sum($replies_count_array);
            $total_comment_count = count($comments) + $sum_of_replies;
    
        }else{
            $total_comment_count = 0;
        }
      
        $final_result = [
            "comments_count" => $total_comment_count,
            "comments" => $comments
        ];
        return $final_result;
    }
}
