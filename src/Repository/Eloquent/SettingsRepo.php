<?php


namespace GeniusSystems\ReviewRating\Repository\Eloquent;



use GeniusSystems\ReviewRating\Models\ReviewSettings;
use GeniusSystems\ReviewRating\Repository\Interfaces\SettingsInterface;

class SettingsRepo extends BaseRepo implements SettingsInterface
{
    private $contentLike;

    public function __construct(ReviewSettings $settings)
    {
        $this->settings = $settings;
        parent::__construct($settings);
       
       
    }
    public function getSettingsData(){
        $data = ReviewSettings::get();
        return $data;
    }
    public function getEnabledFeatures(){
       
        $data = ReviewSettings::where('setting_name','enabled_features')->pluck('value')->first();
        $array_data = explode(",",$data);
        return $array_data;
    }
    public function reviewSetting(){
        $data = ReviewSettings::where('setting_name','only_approved_reviews')->pluck('value')->first();
        
        return $data;
    }
}
