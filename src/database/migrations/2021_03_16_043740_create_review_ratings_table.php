<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('is_approved',[0,1])->default(0);
            $table->double('rating');
            $table->longText('review');
            $table->enum('recommend', ['yes', 'no'])->nullable();
            $table->string('reviewable_type');
            $table->string('reviewable_id');
            $table->string('content_slug');
            $table->unsignedInteger('user_id');
            $table->string('username');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_ratings');
    }
}
