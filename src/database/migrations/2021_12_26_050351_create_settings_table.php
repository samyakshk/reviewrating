<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('setting_name');
            $table->string('value');              
            $table->timestamps();
        });
        $sql = "INSERT INTO `review_settings` (`id`, `setting_name`, `value`, `created_at`, `updated_at`) VALUES (NULL, 'only_approved_reviews', '0', NULL, NULL);";
        $data = \DB::select($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_settings');
    }
}
