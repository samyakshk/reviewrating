<?php

namespace GeniusSystems\ReviewRating;

use Illuminate\Support\ServiceProvider;
use GeniusSystems\ReviewRating\Repository\Eloquent\BaseRepo;
use GeniusSystems\ReviewRating\Repository\Eloquent\CommentRepo;
use GeniusSystems\ReviewRating\Repository\Eloquent\SettingsRepo;
use GeniusSystems\ReviewRating\Repository\Eloquent\ContentLikeRepo;
use GeniusSystems\ReviewRating\Repository\Eloquent\ContentViewRepo;
use GeniusSystems\ReviewRating\Repository\Interfaces\BaseInterface;
use GeniusSystems\ReviewRating\Repository\Eloquent\ReviewRatingRepo;
use GeniusSystems\ReviewRating\Repository\Interfaces\CommentInterface;
use GeniusSystems\ReviewRating\Repository\Interfaces\SettingsInterface;
use GeniusSystems\ReviewRating\Repository\Interfaces\ContentLikeInterface;
use GeniusSystems\ReviewRating\Repository\Interfaces\ContentViewInterface;
use GeniusSystems\ReviewRating\Repository\Interfaces\ReviewRatingInterface;

class ReviewRatingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    	$this->app->bind(BaseInterface::class, BaseRepo::class);
        $this->app->bind(ReviewRatingInterface::class, ReviewRatingRepo::class);
        $this->app->bind(ContentLikeInterface::class, ContentLikeRepo::class);
        $this->app->bind(ContentViewInterface::class, ContentViewRepo::class);
        $this->app->bind(SettingsInterface::class, SettingsRepo::class);
        $this->app->bind(CommentInterface::class, CommentRepo::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFromHere(__DIR__.'/routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }
    protected function loadRoutesFromHere($path)
    {
        // if (! ($this->app instanceof CachesRoutes && $this->app->routesAreCached())) {
            require $path;
        // }
    }
}
