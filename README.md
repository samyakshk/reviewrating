# reviewrating

Package for review,likes and dislikes

## Getting started


Install the package by composer

```
    composer require samyakshk/reviewrating
```

Register provider in bootsrap/app.php

```
    $app->register(ReviewRatingServiceProvider::class);
```


use GeniusSystems\ReviewRating\ReviewRatingServiceProvider;


Migrate Database

```
    php artisan migrate
```
or more specifically
```
php artisan migrate --path=/vendor/samyakshk/reviewrating/src/database/migrations
```
For Endpoints documentation
visit
https://documenter.getpostman.com/view/424388/UVRHhhpo